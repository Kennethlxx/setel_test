//
//  BaseViewModel.swift
//  setel_test
//
//  Created by Kenneth Lee on 04/11/2019.
//  Copyright © 2019 Kenneth Lee. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class BaseViewModel: NSObject {
    let loading = ActivityIndicator()
    let headerLoading = ActivityIndicator()
    let footerLoading = ActivityIndicator()
    let errorTracker = ErrorTracker()
    
    deinit {
        print(String(describing: type(of: self)) + "-deinit")
    }
    
}

protocol ViewModelType {
    associatedtype Input
    associatedtype Output
    
    func transform(input: Input) -> Output
}
