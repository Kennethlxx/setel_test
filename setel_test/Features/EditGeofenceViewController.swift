//
//  EditGeofenceViewController.swift
//  setel_test
//
//  Created by Kenneth Lee on 04/11/2019.
//  Copyright © 2019 Kenneth Lee. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import MapKit

class EditGeofenceViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var rangeLabel: UILabel!
    @IBOutlet weak var rangeSlider: UISlider!
    @IBOutlet weak var currentLocationBtn: UIButton!
    
    lazy var vm = EditGeofenceVM()
    let disposeBag = DisposeBag()
    var callback: ((_ coordinate: CLLocationCoordinate2D, _ radius: Double) -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationController()
        setupMap()
        bindViewModel()
    }
    
    func setupNavigationController() {
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(dismissView))
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor(named: "White")
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Update", style: .plain, target: self, action: #selector(add))
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor(named: "White")
    }
    
    @objc func dismissView() {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    @objc func add() {
        let coordinate = mapView.centerCoordinate
        let radius = vm.rangeSelection.value
        print(coordinate)
        self.callback?(coordinate, Double(radius))
        dismissView()
    }
    
    func setupMap() {
        mapView.delegate = self
        mapView.zoomToUserLocation()
    }
    
    func bindViewModel() {
        rangeSlider.rx.value.bind(to: vm.rangeSelection)
            .disposed(by: self.disposeBag)
        vm.rangeSelection.map { "\($0)" }.bind(to: rangeLabel.rx.text).disposed(by: self.disposeBag)
        currentLocationBtn.rx.tap.throttle(RxTimeInterval.seconds(Int(0.3)), scheduler: MainScheduler.instance)
            .bind { [weak self](_) in
                guard let `self` = self else { return }
                self.mapView.zoomToUserLocation()
        }.disposed(by: self.disposeBag)
    }

}

extension EditGeofenceViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let identifier = "myGeotification"
        if annotation is GeoLocationModel {
            var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKPinAnnotationView
            if annotationView == nil {
                annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            } else {
                annotationView?.annotation = annotation
            }
            return annotationView
        }
        return nil
    }
    
}
