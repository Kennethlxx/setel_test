//
//  EditGeofenceVM.swift
//  setel_test
//
//  Created by Kenneth Lee on 04/11/2019.
//  Copyright © 2019 Kenneth Lee. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift
import CoreLocation

class EditGeofenceVM: BaseViewModel, ViewModelType {
    
    let rangeSelection = BehaviorRelay<Float>.init(value: 50.0)
    
    struct Input {
        
    }
    
    struct Output {
        
    }
    
    func transform(input: EditGeofenceVM.Input) -> EditGeofenceVM.Output {
        
        return Output()
    }
    
}
