//
//  MainVM.swift
//  setel_test
//
//  Created by Kenneth Lee on 04/11/2019.
//  Copyright © 2019 Kenneth Lee. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift
import CoreLocation

class MainVM: BaseViewModel, ViewModelType {
    
//    let destinationModel = BehaviorRelay<GeoLocationModel>.init(value: Constant.destinationModel)
    let disposeBag = DisposeBag()
    
    struct Input {
        let updateDestination: Observable<GeoLocationModel>
    }
    
    struct Output {
        let geoLocation: BehaviorRelay<GeoLocationModel>
        let withinRange: BehaviorRelay<String>
        let removeOldDestination: PublishSubject<GeoLocationModel>
    }
    
    func transform(input: MainVM.Input) -> MainVM.Output {
        let geoLocation =  BehaviorRelay<GeoLocationModel>.init(value: Constant.destinationModel)
        let withinRange = BehaviorRelay<String>.init(value: "")
        let removeOldDestination = PublishSubject<GeoLocationModel>()
        
        input.updateDestination.subscribe(onNext: { [weak self](model) in
            guard let `self` = self else { return }
            removeOldDestination.onNext(geoLocation.value)
            LocationManager.shared.stopMonitoring(geotification: geoLocation.value)
            geoLocation.accept(model)
        }).disposed(by: self.disposeBag)
        
        geoLocation.accept(Constant.destinationModel)
        return Output(geoLocation: geoLocation, withinRange: withinRange, removeOldDestination: removeOldDestination)
    }
    
}

