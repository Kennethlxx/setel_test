//
//  MainViewController.swift
//  setel_test
//
//  Created by Kenneth Lee on 04/11/2019.
//  Copyright © 2019 Kenneth Lee. All rights reserved.
//

import UIKit
import MapKit
import RxSwift
import RxCocoa

class MainViewController: UIViewController {
    
    @IBOutlet weak var radiusLabel: UILabel!
    @IBOutlet weak var myLocationBtn: UIButton!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var selectAreaBtn: UIButton!
    
    lazy var vm = MainVM()
    let disposeBag = DisposeBag()
    let updateDestination = PublishSubject<GeoLocationModel>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bindViewModel()
    }
    
    func bindViewModel() {
        let input = MainVM.Input(updateDestination: updateDestination)
        let output = vm.transform(input: input)
        mapView.delegate = self
        output.geoLocation.asObservable().subscribe(onNext: { [weak self](model) in
            guard let `self` = self else { return }
            self.add(model)
            self.startMonitoring(geotification: model)
        }).disposed(by: self.disposeBag)
        output.removeOldDestination.subscribe(onNext: { [weak self](oldModel) in
            guard let `self` = self else { return }
            self.remove(oldModel)
        }).disposed(by: self.disposeBag)
        
        myLocationBtn.rx.tap.throttle(RxTimeInterval.seconds(Int(0.3)), scheduler: MainScheduler.instance)
            .bind { [weak self](_) in
                guard let `self` = self else { return }
                self.zoomToCurrentLocation()
        }.disposed(by: self.disposeBag)
        
        selectAreaBtn.rx.tap.throttle(RxTimeInterval.seconds(Int(0.3)), scheduler: MainScheduler.instance)
            .bind { [weak self](_) in
                guard let `self` = self else { return }
                let vc = EditGeofenceViewController()
                let navigationController = UINavigationController(rootViewController: vc)
                vc.callback = { [weak self] (coordinate, radius) in
                    let clampedRadius = min(radius, LocationManager.shared.locationManager.maximumRegionMonitoringDistance)
                    let geotification = GeoLocationModel(coordinate: coordinate, radius: clampedRadius, identifier: Constant.definedKey, note: "Geofence Location", eventType: .onEntry)
                    self?.updateDestination.onNext(geotification)
                }
                self.present(navigationController, animated: true, completion: nil)
        }.disposed(by: self.disposeBag)
        
        LocationManager.shared.currentLocation.subscribe(onNext: { [weak self](location) in
            guard let `self` = self else { return }
            self.zoomToCurrentLocation()
        }).disposed(by: self.disposeBag)
        
        LocationManager.shared.inRadius.subscribe(onNext: { [weak self](status) in
            guard let `self` = self else { return }
            self.radiusLabel.text = status ? "In Radius" : "Out of Radius"
        }).disposed(by: self.disposeBag)
    }
    
    
    // MARK: Functions that update the model/associated views with geotification changes
    func add(_ geoLocationModel: GeoLocationModel) {
        mapView.addAnnotation(geoLocationModel)
        addRadiusOverlay(forGeotification: geoLocationModel)
    }
    
    func remove(_ geotification: GeoLocationModel) {
      mapView.removeAnnotation(geotification)
      removeRadiusOverlay(forGeotification: geotification)
    }
    
    // MARK: Map overlay functions
    func addRadiusOverlay(forGeotification geoLocationModel: GeoLocationModel) {
        mapView?.addOverlay(MKCircle(center: geoLocationModel.coordinate, radius: geoLocationModel.radius))
    }
    
    func removeRadiusOverlay(forGeotification geotification: GeoLocationModel) {
        // Find exactly one overlay which has the same coordinates & radius to remove
        guard let overlays = mapView?.overlays else { return }
        for overlay in overlays {
            guard let circleOverlay = overlay as? MKCircle else { continue }
            let coord = circleOverlay.coordinate
            if coord.latitude == geotification.coordinate.latitude && coord.longitude == geotification.coordinate.longitude && circleOverlay.radius == geotification.radius {
                mapView?.removeOverlay(circleOverlay)
                break
            }
        }
    }
    
    // MARK: Other mapview functions
    func zoomToCurrentLocation() {
        mapView.zoomToUserLocation()
    }
    
    func region(with geotification: GeoLocationModel) -> CLCircularRegion {
        let region = CLCircularRegion(center: geotification.coordinate, radius: geotification.radius, identifier: geotification.identifier)
        region.notifyOnEntry = true
        region.notifyOnExit = true
        return region
    }
    
    func startMonitoring(geotification: GeoLocationModel) {
        if !CLLocationManager.isMonitoringAvailable(for: CLCircularRegion.self) {
            showAlert(withTitle:"Error", message: "Geofencing is not supported on this device!")
            return
        }
        
        if CLLocationManager.authorizationStatus() != .authorizedAlways {
            let message = """
        Your geotification is saved but will only be activated once you grant
        Geotify permission to access the device location.
        """
            showAlert(withTitle:"Warning", message: message)
        }
        
        let fenceRegion = region(with: geotification)
        LocationManager.shared.locationManager.startMonitoring(for: fenceRegion)
        LocationManager.shared.locationManager.requestState(for: fenceRegion)
        LocationManager.shared.lastCacheRegion.accept(fenceRegion)
    }
    
}

extension MainViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let identifier = "myGeotification"
        if annotation is GeoLocationModel {
            var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKPinAnnotationView
            if annotationView == nil {
                annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            } else {
                annotationView?.annotation = annotation
            }
            return annotationView
        }
        return nil
    }
    
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay is MKCircle {
            let circleRenderer = MKCircleRenderer(overlay: overlay)
            circleRenderer.lineWidth = 1.0
            circleRenderer.strokeColor = .purple
            circleRenderer.fillColor = UIColor.purple.withAlphaComponent(0.4)
            return circleRenderer
        }
        return MKOverlayRenderer(overlay: overlay)
    }
    
}
