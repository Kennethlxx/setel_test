//
//  Constant.swift
//  setel_test
//
//  Created by Kenneth Lee on 04/11/2019.
//  Copyright © 2019 Kenneth Lee. All rights reserved.
//

import Foundation
import CoreLocation

struct Constant {
    static let fixedBssid = "f4:8c:eb:5:6a:d2"
    static let definedKey = "Defined Location"
    static let destinationLocation = CLLocationCoordinate2D(latitude: 3.1286886, longitude: 101.7243132)
    static let destinationModel = GeoLocationModel(coordinate: Constant.destinationLocation, radius: 50, identifier: definedKey, note: "Geofence Location", eventType: .onEntry)
}
