//
//  Utilities.swift
//  setel_test
//
//  Created by Kenneth Lee on 04/11/2019.
//  Copyright © 2019 Kenneth Lee. All rights reserved.
//

import Foundation
import MapKit

// MARK: Helper Extensions
extension UIViewController {
  func showAlert(withTitle title: String?, message: String?) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
    alert.addAction(action)
    present(alert, animated: true, completion: nil)
  }
}

extension MKMapView {
  func zoomToUserLocation() {
    guard let coordinate = LocationManager.shared.locationManager.location?.coordinate else { return }
    let region = MKCoordinateRegion(center: coordinate, latitudinalMeters: 1000, longitudinalMeters: 1000)
    setRegion(region, animated: true)
  }
}
