//
//  LocationManager.swift
//  setel_test
//
//  Created by Kenneth Lee on 04/11/2019.
//  Copyright © 2019 Kenneth Lee. All rights reserved.
//

import Foundation
import CoreLocation
import RxSwift
import RxCocoa
import Reachability

class LocationManager: NSObject {
    
    static let shared = LocationManager()
    let locationManager = CLLocationManager()
    let currentLocation = BehaviorRelay<CLLocationCoordinate2D?>.init(value: nil)
    let lastCacheRegion = BehaviorRelay<CLCircularRegion?>.init(value: nil)
    let inRadius = BehaviorRelay<Bool>.init(value: false)
    var timer = Timer()
    let reachability = Reachability()!
    
    func initialize() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        checkLocationAuthorizationStatus()
        networkStatusListener()
    }
    
    func startTimer() {
        timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(validateUserInRange), userInfo: nil, repeats: false)
    }
    
    func stopTimer() {
        timer.invalidate()
    }
    
    func checkLocationAuthorizationStatus() {
        if CLLocationManager.authorizationStatus() == .authorizedAlways {
            
        } else {
            locationManager.requestAlwaysAuthorization()
        }
    }
    
    @objc func validateUserInRange() {
        checkUserIsConnectedWifi()
    }
    
    private func checkUserIsConnectedWifi() {
        stopTimer()
        
        if let wifi = getWifiInfo().first {
            inRadius.accept(wifi.bssid == Constant.fixedBssid ? true : false)
            wifi.bssid != Constant.fixedBssid ? startTimer() : nil
        } else {
            inRadius.accept(false)
            startTimer()
        }
    }
    
}

//Reachability
extension LocationManager {
    
    func networkStatusListener() {
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(note:)), name: .reachabilityChanged, object: reachability)
        do{
            try reachability.startNotifier()
        } catch{
            print("could not start reachability notifier")
        }
    }
    
    @objc func reachabilityChanged(note: NSNotification) {
        guard let region = lastCacheRegion.value else { return }
        locationManager.requestState(for: region)
    }
    
}


//CLLocation
extension LocationManager: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last{
            currentLocation.accept(CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude))
            
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didDetermineState state: CLRegionState, for region: CLRegion) {
        stopTimer()
        
        switch state {
        case .inside:
            inRadius.accept(true)
        case .outside:
            checkUserIsConnectedWifi()
        case .unknown:
            inRadius.accept(false)
            startTimer()
        }
        
        print(region)
    }
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        if region is CLCircularRegion {
            inRadius.accept(true)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        if region is CLCircularRegion {
            checkUserIsConnectedWifi()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, monitoringDidFailFor region: CLRegion?, withError error: Error) {
        print("Monitoring failed for region with identifier: \(region!.identifier)")
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Location Manager failed with the following error: \(error)")
    }
    
    func stopMonitoring(geotification: GeoLocationModel) {
      for region in locationManager.monitoredRegions {
        guard let circularRegion = region as? CLCircularRegion, circularRegion.identifier == geotification.identifier else { continue }
        locationManager.stopMonitoring(for: circularRegion)
      }
    }
    
}

